

## Setup

```bash
sudo apt update
sudo apt upgrade -y
sudo apt install -y \
    pigpio \
    python3-pip

sudo systemctl enable pigpiod
sudo systemctl start pigpiod
pip3 install paho-mqtt \
    python-statemachine \
    httplib2 \
    async-timeout
```

`sudo nano /etc/rc.local`
```bash
/home/pi/app/commands-subscribe.py &
/sbin/iw wlan0 set power_save off
```