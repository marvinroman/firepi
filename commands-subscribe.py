#!/usr/bin/python3

import json
import logging
import os
import time
import httplib2

from dotenv import dotenv_values
from gpiozero import OutputDevice
from paho.mqtt import client as mqtt_client
# from pprint import pprint

# Setup logging
logging.basicConfig(format='%(asctime)s %(message)s',
                    filename='firepi.log', level=logging.DEBUG)

config = dotenv_values(".env")

publishes = {
    "heat_range": {
        "topic": "fireplace/heat_range/state",
        "retain": True,
    }, "power_command": {
        "topic": "fireplace/power/set",
        "retain": True
    }, "power_status": {
        "topic": "fireplace/power/status",
        "retain": True,
    }
}
subscribes = {
    "auger_control": {
        "topic": "fireplace/auger/control"
    }, "current_temperature": {
        "topic": "climate/temperature/smoothed"
    }, "heat_range": {
        "topic": "fireplace/heat_range/control"
    }, "power_command": {
        "topic": "fireplace/power/set"
    }, "power_status": {
        "topic": "fireplace/power/status"
    }, "temperature_range": {
        "topic": "climate/temperature/set"
    }
}


class Broker:
    """instance of mqtt broker connection

    Returns:
        _type_: _description_
    """
    client = None
    client_id = "mqtt.firepi.local"
    username = config.get("MQTT_USER")
    password = config.get("MQTT_PASSWORD")
    host = config.get("MQTT_HOST")
    port = config.get("MQTT_PORT")
    default_qos = 2

    def __init__(self, fireplace):
        self.fireplace = fireplace
        self.fireplace.set_broker(self)
        self.client = self.connect_mqtt()
        self.handle_subscriptions()
        self.client.loop_forever()

    def connect_mqtt(self):
        """Connect to MQTT broker
        """
        def on_connect(client, userdata, flags, rc):
            """listener for when MQTT client conencts

            Args:
                client (client): the client instance for this callback
                userdata (object): the private user data as set in Client() or user_data_set()
                flags (dict): response flags sent by the broker
                rc (int): the connection result
            """
            if rc == 0:
                subscription_topics = [
                    (subscribes["auger_control"]["topic"], 2),
                    (subscribes["current_temperature"]["topic"], 2),
                    (subscribes["heat_range"]["topic"], 2),
                    (subscribes["power_command"]["topic"], 2),
                    (subscribes["power_status"]["topic"], 2),
                    (subscribes["temperature_range"]["topic"], 2),
                ]
                self.client.subscribe(subscription_topics)
                logging.info("Connected to MQTT Broker!")
            else:
                logging.error("Failed to connect, return code %d\n", rc)
        client = mqtt_client.Client(self.client_id)
        client.username_pw_set(self.username, self.password)
        client.on_connect = on_connect
        client.connect(self.host, int(self.port))
        return client

    # method to publish to MQTT topics
    def publish(self, topic, msg, do_retain):
        """Pulishes message to MQTT Broker

        Args:
            topic (string): topic to publish message to
            msg (string): message to send to the topic
            do_retain (boolean): whether to signal to retain the message
        """
        result = self.client.publish(topic, payload=msg, qos=self.default_qos, retain=do_retain)
        status = result[0]
        if status == 0:
            logging.info("Send %s to topic %s", msg, topic)
        else:
            logging.error("Failed to send message to topic %s", topic)

    def handle_subscriptions(self):
        """handle subscriptions
        """
        def on_message(client, userdata, message):
            """catches messages when they are received

            Args:
                client (_type_): _description_
                userdata (_type_): _description_
                message (_type_): _description_
            """
            self.fireplace.handle_new_message(message)
        self.client.on_message = on_message


class Stove:
    """instance of stove methods
    """
    # State variables
    broker = None
    power_state = None
    power_status = None
    heat_range_state = 0
    intialized_topics = []
    temperature_range = {"min": 0, "max": 100}
    current_temperature = None
    temperature_low_variance = 1.5
    minutes_between_checks = 20
    last_change_time = time.time()

    # Setup keys
    on_key = OutputDevice("BOARD16", False)
    off_key = OutputDevice("BOARD18", False)
    auger_delay_key = OutputDevice("BOARD36", False)
    heat_range_up_key = OutputDevice("BOARD40", False)
    heat_range_down_key = OutputDevice("BOARD38", False)

    def getStatus(self):
        http = httplib2.Http()
        api_url = "http://{host}:1880/api/global/status".format(host=config.get("MQTT_HOST"))
        http_response = http.request(api_url)
        json_response = json.loads(http_response[1])
        self.current_temperature = float(json_response.get("temperature"))
        self.heat_range_state = int(json_response.get("heat_range"))
        self.power_state = json_response.get("power_state")
        self.power_status = json_response.get("power_status")
        self.temperature_range = {
            "max": float(json_response.get("max")), "min": float(json_response.get("min"))
        }

    def set_broker(self, broker):
        """assign the broker so it can be called for publishing messages

        Args:
            broker (class): broker class instance
        """
        self.broker = broker

    def handle_current_temperature(self, payload):
        """handle new temperatures

        Args:
            payload (float): temperature as a float
        """
        current_time = time.time()
        self.getStatus()
        self.current_temperature = float(payload)
        max_temp = float(self.temperature_range.get("max"))
        min_temp = float(self.temperature_range.get("min"))
        logging.debug("current_temperature: %s, power_state: %s, max: %s, min: %s, power_status: %s", self.current_temperature, self.power_state, max_temp, min_temp, self.power_status)
        if self.power_state == "Auto" and self.temperature_range and self.power_status:
            if self.current_temperature > max_temp and self.power_status == "On":
                self.stop_stove()
            elif self.current_temperature < min_temp and self.power_status == "Off":
                self.start_stove()
            elif (current_time - self.last_change_time) > (60 * self.minutes_between_checks):
                self.test_temp_difference()
                self.last_change_time = time.time()

    def handle_power_command(self, payload):
        """handles new power command

        Args:
            payload (string): command sent regarding new power state
        """
        # if mode is switched to and and stove is on then turn it off
        if payload == "Off" and self.power_status == "On":
            self.stop_stove()
        elif payload == "On" and self.power_status == "Off":
            self.start_stove()
        self.power_state = payload

    def handle_new_message(self, message):
        """receives messages from mqtt broker

        Args:
            message (object): message and payload
        """

        payload = message.payload.decode()

        # handle messages after first one is received
        if message.topic in self.intialized_topics:
            logging.info("Received %s from %s", payload, message.topic)
            # mode topic controls modes possibly values (heat, OFF)
            if message.topic == subscribes["power_command"]["topic"]:
                self.handle_power_command(payload)
            elif message.topic == subscribes["auger_control"]["topic"]:
                self.auger_delay()
            elif message.topic == subscribes["current_temperature"]["topic"]:
                self.handle_current_temperature(payload)
            elif message.topic == subscribes["temperature_range"]["topic"]:
                self.temperature_range = json.loads(payload)
            elif message.topic == subscribes["power_status"]["topic"]:
                self.power_status = payload
            elif message.topic == subscribes["heat_range"]["topic"]:
                set_point = int(payload)
                if set_point > self.heat_range_state:
                    self.heat_range_up(set_point)
                elif set_point < self.heat_range_state:
                    self.heat_range_down(set_point)
        else:
            self.intialized_topics.append(message.topic)
            if message.topic == subscribes["power_command"]["topic"]:
                self.power_state = payload
            elif message.topic == subscribes["current_temperature"]["topic"]:
                self.current_temperature = payload
            elif message.topic == subscribes["temperature_range"]["topic"]:
                self.current_temperature = json.loads(payload)
            elif message.topic == subscribes["power_status"]["topic"]:
                self.power_status = payload
            elif message.topic == subscribes["heat_range"]["topic"]:
                self.heat_range_state = int(payload)

    def auger_delay(self):
        """triggers auger delay button
        """
        if self.power_status == "On":
            logging.debug('Auger delay')
            time.sleep(1)
            self.auger_delay_key.on()  # initiate Auger Delay key press
            time.sleep(0.5)
            self.auger_delay_key.off()  # pullup Auger Delay key press

    def heat_range_down(self, set_point):
        """move heat range down

        Args:
            set_point (int): heat range setting to change the stove to
        """

        if self.power_status == "On":
            logging.info("Heat range down.  heat_range_state: %s, set_point: %s", self.heat_range_state, set_point)
            # Loop through until the current heat range setting is no longer greater than the set point
            while set_point < self.heat_range_state and self.heat_range_state > 1:
                self.heat_range_down_key.on()  # initiate the keypress of the heat level down button
                time.sleep(0.5)
                self.heat_range_down_key.off()  # pull up the key for level down
                self.last_change_time = time.time()  # reset the last changed time
                if self.heat_range_state > 1:
                    self.heat_range_state = self.heat_range_state - 1
                else:
                    self.heat_range_state = 5
                logging.info("Turning heat down, heat_range_state: %s", self.heat_range_state)
                time.sleep(2)
            self.broker.publish(publishes["heat_range"]["topic"], self.heat_range_state, publishes["heat_range"]["retain"])

    def heat_range_up(self, set_point):
        """updates the heat range settings

        Args:
            set_point (int): heat range set point
        """
        if self.power_status == "On":
            logging.info("Heat range up.  heat_range_state: %s, set_point: %s", self.heat_range_state, set_point)
            # Loop through until the current heat range setting is no longer lower than the set point
            while self.heat_range_state < set_point and self.heat_range_state < 5:
                self.heat_range_up_key.on()  # initiate keypress of the heat level up button
                time.sleep(0.5)
                self.heat_range_up_key.off()  # pull up the key for heat lever up button
                self.last_change_time = time.time()  # reset the last changed time
                if self.heat_range_state < 5:
                    self.heat_range_state = self.heat_range_state + 1
                else:
                    self.heat_range_state = 5  # raise heat lever by 1
                logging.info("Turning heat up, heat_range_state: %s", self.heat_range_state)
                time.sleep(2)
            self.broker.publish( publishes["heat_range"]["topic"], self.heat_range_state, publishes["heat_range"]["retain"])

    def start_stove(self):
        """sends IO signals to the stove keys to start the stove
        """
        self.broker.publish(publishes["power_status"]["topic"], "Turning On", publishes["power_status"]["retain"])
        logging.info('Starting stove')
        time.sleep(1)
        self.on_key.on()  # initiate ON key press
        time.sleep(0.5)
        self.on_key.off()  # pullup ON key press
        # send power state of ON back to MQTT
        self.last_change_time = time.time()  # reset stored time to now
        self.heat_range_state = 1

    # method to stop the stove
    def stop_stove(self):
        """sends IO signals to the stove keys to stop the stove
        """
        self.broker.publish(publishes["power_status"]["topic"], "Turning Off", publishes["power_status"]["retain"])
        logging.info('Stopping stove')
        time.sleep(1)
        self.heat_range_state = 5  # bump range state all the way up to take into account user altering the range via the panel this will force the heat range to be pushed all the way down irregardless of current setting
        # move heat range down to 1 before turning off
        self.heat_range_down(1)
        self.off_key.on()  # initiate OFF key press
        time.sleep(0.5)
        self.off_key.off()  # pullup OFF key press

    def test_temp_difference(self):
        """when a set number of minutes has lapped and the temperature is below the bottom threshold + variance then turn up the heat level
        """
        temperature_diff = float(self.temperature_range.get("min")) - float(self.current_temperature)
        logging.info("Testing temperature difference. temp_diff: %s, current_temp: %s, temperature_state: %s, temperature_low_variance: %s, heat_range_state: %s",
                     temperature_diff, self.current_temperature, self.temperature_range.get("min"), self.temperature_low_variance, self.heat_range_state)
        # If on and heat range is greater than 5 turn it down since it shouldn't run on level 5 for more than 30 mins
        if self.power_status == "On" and self.heat_range_state > 3:
            self.heat_range_down(1)
        # If on and the temperature difference between the set point and current is greater than the variance then raise the heat level
        elif self.power_status == "On" and temperature_diff > self.temperature_low_variance:
            self.heat_range_up(self.heat_range_state + 1)
        # else if on and heat range is greater than 1 then lower since the temperature is within the variance
        elif self.power_status == "On" and self.heat_range_state > 1:
            self.heat_range_down(self.heat_range_state - 1)


# make sure home assisstant core is pingable before attempting connection to MQTT server
while (True):
    response = os.system("ping -c 1 " + config.get("MQTT_HOST"))
    if response == 0:
        break
    else:
        time.sleep(5)


def run():
    """initialize the MQTT client
    """
    fireplace = Stove()
    fireplace.getStatus()
    Broker(fireplace)


if __name__ == '__main__':
    run()
